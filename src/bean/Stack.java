package bean;

public class Stack {
	private Node root;
	private Node top;
	private int size;
	private static int maxSize = 1000;

	public Stack() {
		this.root = null;
		this.top = null;
		this.size = 0;
	}

	public boolean isEmpty() {
		return size == 0;
	}

	public int top() {
		if (isEmpty()) {
			try {
				throw new Exception("La pile est vide");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return this.top.getData();
	}

	public void push(int elem) throws Exception {
		if (size == maxSize) {
			
				throw new Exception("La pile a atteint sa taille maximale de " + maxSize + " elements");
			
		}
		Node toAdd = new Node(elem);
		if (isEmpty()) {
			this.root = toAdd;
		}
		this.top = toAdd;

		Node lastNode = root;
		for (int i = 0; i < this.size - 1; i++) {
			lastNode = lastNode.getNext();
		}
		lastNode.setNext(toAdd);
		this.size++;
	}

	public int pop() throws Exception {
		if (isEmpty()) {
			throw new Exception("La pile est vide");
		}

		int value = top();
		Node currentNode = root;
		for (int i = 0; i < size - 1; i++) {
			if (currentNode.getNext() == top) {
				value = top.getData();
				currentNode.setNext(null);
				this.top = currentNode;
				this.size--;
				break;
			}
			currentNode = currentNode.getNext();
		}
		
		return value;
	}

	public int elementAt(int pos) throws Exception {
		if (pos + 1 > size) {
			
				throw new Exception("Il n'y a aucun �l�ment � cette position");
			
		}

		int value = 0;
		Node currentNode = root;
		for (int i = 0; i < this.size; i++) {
			if (i == pos) {
				value = currentNode.getData();
				break;
			}
			currentNode = currentNode.getNext();
		}
		return value;
	}

	public Stack clone() {
		Stack clone = new Stack();
		if(!isEmpty()){
			Node currentNode = root;
			for(int i = 0; i < this.size; i++){
					try {
						clone.push(currentNode.getData());
					} catch (Exception e) {
						e.printStackTrace();
					}
				currentNode = currentNode.getNext();
			}
		}
		return clone;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == null){
			return false;
		}
		if(!(obj instanceof Stack)){
			return false;
		}
		
		Stack other = (Stack) obj;
		if(isEmpty() && other.isEmpty()){
			return true;
		}
		if(this.size != other.size){
			return false;
		}
		
		
		Node currentNode = root;
		Node otherCurrentNode = other.root;
		for(int i = 0; i < this.size; i++){
			if(currentNode.getData() != otherCurrentNode.getData()){
				return false;
			}
			currentNode = currentNode.getNext();
			otherCurrentNode = otherCurrentNode.getNext();
		}
		 return true;
	}

	public static int getMaxSize() {
		return maxSize;
	}
}
