package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import bean.Stack; 
 
public class StackTest {  
	Stack stack; 
 
	@Before  
	public void setUp() throws Exception {
		stack = new Stack();
	}
 
	@Test 
	public void testIsEmpty() throws Exception {
		assertTrue(stack.isEmpty());
		fillStack(); 
		assertFalse(stack.isEmpty());
	}
	
	@Test (expected = Exception.class)
	public void testTopException() {
		stack.top();
	}

	@Test
	public void testTop() throws Exception {
		fillStack();
		assertTrue(stack.top() == 2);
	}
	
	@Test (expected = Exception.class)
	public void testPushException() throws Exception{
		stack.elementAt(0);
		fillStack();
		stack.elementAt(3);
	}
	
	@Test (expected = Exception.class)
	public void testPushMax() throws Exception {//ici
		maxStack();
		stack.push(1);
	}

	@Test
	public void testPush() throws Exception {
		fillStack();
		assertTrue(stack.elementAt(1) == 2);
	}
	
	@Test (expected = Exception.class)
	public void testPopException() throws Exception{
		stack.pop();
	}

	@Test
	public void testPop() throws Exception {
		fillStack();
		stack.pop();
		assertTrue(stack.top() == 1);
		
		stack = new Stack();
		stack.push(1);
		assertTrue(stack.top() == 1);
	}
	
	@Test
	public void testPopMax() throws Exception {
		maxStack();
		stack.pop();
		assertTrue(stack.top() == 998);
	}
	
	@Test (expected = Exception.class)
	public void testElementAtException() throws Exception {
		stack.elementAt(0);
		fillStack();
		stack.elementAt(5);
	}

	@Test
	public void testElementAt() throws Exception {
		fillStack();
		assertTrue(stack.elementAt(0) == 1);
		assertTrue(stack.elementAt(1) == 2);
	}

	@Test
	public void testClone() throws Exception {
		assertTrue(stack.equals(stack.clone()));
		fillStack();
		assertTrue(stack.equals(stack.clone()));
	}
	
	@Test
	public void testEquals() throws Exception{
		assertTrue(stack.equals(stack.clone()));
		assertFalse(stack.equals(new String()));
		assertFalse(stack.equals(null));
		
		Stack stack2 = new Stack();
		fillStack();
		assertFalse(stack.equals(stack2));
		
		stack2.push(1);
		assertFalse(stack.equals(stack2));
		
		stack = new Stack();
		assertFalse(stack.equals(stack2));
		
		stack2.push(1);
		fillStack();
		assertFalse(stack.equals(stack2));
	}
	
	private void fillStack() throws Exception{
		stack.push(1);
		stack.push(2);
	}
	
	private void maxStack() throws Exception{
		for(int i = 0; i < Stack.getMaxSize() ; i++){
			stack.push(i);
		}
	}
}
